package br.com.devmedia.rest;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.devmedia.dao.NotaDAO;
import br.com.devmedia.entidade.Nota;

@Path("/notas")
public class NotasService {

	private static final String CHARSET_UTF8 = ";charset=utf-8";
	
	private NotaDAO dao;
	
	@PostConstruct
	private void init() {
		dao = new NotaDAO();
	}
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Nota> getAll() {
		List<Nota> notas = null;
		try {
			notas = dao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notas;
	}
	
	@GET
	@Path("/get/{id}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Nota getByID(@PathParam("id") int ID) {
		Nota nota = null;
		try {
			nota = dao.getByID(ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nota;
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.TEXT_PLAIN)
	public String add(Nota nota) {
		String msg = "";
		try {
			int newID = dao.add(nota); 
			
			msg = "Nota: " + String.valueOf(newID) + " adicionada com sucesso!";
		} catch (Exception e) {
			msg = "Erro ao adicionar nota!";
			e.printStackTrace();
		}
		return msg;
	}
	
	@PUT
	@Path("/edit/{id}")
	@Consumes(MediaType.APPLICATION_JSON + CHARSET_UTF8)
	@Produces(MediaType.TEXT_PLAIN)
	public String edit(Nota nota, @PathParam("id") int ID) {
		String msg = "";
		try {
			dao.edit(nota, ID);
			msg = "Nota atualizada com sucesso!";
		} catch (Exception e) {
			msg = "Erro ao tentar atualizar!";
			e.printStackTrace();
		}
		return msg;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String delete(@PathParam("id") int ID) {
		String msg = "";
		try {
			dao.remove(ID);
			msg = "Nota deletada com sucesso!";
		} catch (Exception e) {
			msg = "Erro ao tentar deletar!";
			e.printStackTrace();
		}
		return msg;
	}
	
}