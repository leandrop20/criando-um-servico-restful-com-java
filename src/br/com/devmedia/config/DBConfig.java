package br.com.devmedia.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConfig {

	private static String db = "jdbc:mysql://localhost:3306/restfull1";
	private static String user = "root";
	private static String password = "";
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(db, user, password);
	}
	
}
