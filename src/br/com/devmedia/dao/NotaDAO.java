package br.com.devmedia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.com.devmedia.config.DBConfig;
import br.com.devmedia.entidade.Nota;

public class NotaDAO {

	public List<Nota> getAll() throws Exception {
		List<Nota> lista = new ArrayList<Nota>();
		
		Connection conn = DBConfig.getConnection();
		String sql = "SELECT * FROM notas";
		PreparedStatement statement = conn.prepareStatement(sql);
		ResultSet rs = statement.executeQuery();
		
		while (rs.next()) {
			Nota nota = new Nota();
			nota.setId(rs.getInt("id"));
			nota.setTitulo(rs.getString("titulo"));
			nota.setDescricao(rs.getString("descricao"));
			
			lista.add(nota);
		}
		return lista;
	}
	
	public Nota getByID(int ID) throws Exception {
		Nota nota = null;
		
		Connection conn = DBConfig.getConnection();
		String sql = "SELECT * FROM notas WHERE id = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, ID);
		ResultSet rs = statement.executeQuery();
		
		if (rs.next()) {
			nota = new Nota();
			nota.setId(rs.getInt("id"));
			nota.setTitulo(rs.getString("titulo"));
			nota.setDescricao(rs.getString("descricao"));
		}
		
		return nota;
	}
	
	public int add(Nota nota) throws Exception {
		int newID = 0;
		Connection conn = DBConfig.getConnection();
		
		String sql = "INSERT INTO notas (titulo, descricao)"
				+ " VALUES (?, ?)";
		
		PreparedStatement statement = conn.prepareStatement(
			sql, Statement.RETURN_GENERATED_KEYS
		);
		statement.setString(1, nota.getTitulo());
		statement.setString(2, nota.getDescricao());
		statement.execute();
		
		ResultSet rs = statement.getGeneratedKeys();
		if (rs.next()) {
			newID = rs.getInt(1);
		}
		return newID;
	}
	
	public void edit(Nota nota, int ID) throws Exception {
		Connection conn = DBConfig.getConnection();
		
		String sql = "UPDATE notas SET titulo = ?, descricao = ?"
				+ " WHERE id = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, nota.getTitulo());
		statement.setString(2, nota.getDescricao());
		statement.setInt(3, ID);
		statement.execute();
	}
	
	public void remove(int ID) throws Exception {
		Connection conn = DBConfig.getConnection();
		
		String sql = "DELETE FROM notas WHERE id = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setInt(1, ID);
		statement.execute();
	}
	
}